package a_project_nopcommerce;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.nopcommerce.utility.WaitElement;

public class CustomersPage 
{
	WebDriver Driver;
	WaitElement wt;
	@FindBy(xpath="//span[contains(text(),'Customers')]")WebElement customer;
	@FindBy(xpath="//li[@class='treeview menu-open']//ul[@class='treeview-menu']//li//span[@class='menu-item-title'][contains(text(),'Customers')]")WebElement childcustomers;
	@FindBy(xpath="//a[@class='btn bg-blue']")WebElement addnew;
	@FindBy(xpath="//input[@id='Email']")WebElement email;
	@FindBy(xpath="//input[@id='Password']")WebElement password;
	@FindBy(xpath="//input[@id='FirstName']")WebElement firstname;
	@FindBy(xpath="//input[@id='LastName']")WebElement lastname;
	@FindBy(xpath="//input[@id='Gender_Male']")WebElement male;
	@FindBy(xpath="//input[@id='Gender_Female']")WebElement female;
	@FindBy(xpath="//input[@id='DateOfBirth']")WebElement dob;
	@FindBy(xpath="//input[@id='Company']")WebElement company;
	@FindBy(xpath="//div[@class='k-multiselect-wrap k-floatwrap']")WebElement select;
	@FindBy(xpath="//select[@id='VendorId']")WebElement vendor;
	@FindBy(xpath="//button[@name='save']")WebElement save;
	@FindBy(xpath="//input[@id='SearchCompany']")WebElement searchcompany;
	@FindBy(xpath="//input[@id='SearchFirstName']")WebElement seachfirstname;
	@FindBy(xpath="//input[@id='SearchLastName']")WebElement seachlastname;
	@FindBy(xpath="//button[@id='search-customers']")WebElement searchbtn;
	@FindBy(xpath="//span[contains(text(),'Customer roles')]")WebElement customerole;
	@FindBy(xpath="//a[@class='btn bg-blue']")WebElement addrole;
	@FindBy(xpath="//input[@id='Name']")WebElement rolename;
	@FindBy (xpath="//input[@id='FreeShipping']")WebElement roleshipping;
	@FindBy (xpath="//input[@id='TaxExempt']")WebElement taxrole;
	@FindBy(xpath="//button[@name='save']")WebElement savebtn;
	@FindBy(xpath="//input[@id='SearchEmail']")WebElement searchemail;
	
	public CustomersPage(WebDriver driver)
	{
		this.Driver=driver;
	}
	
	public boolean CustomerOption() throws InterruptedException
	{
		WaitElement.WaitForElement(customer, 300, Driver);
		if(customer.isDisplayed())
		{
			customer.click();
			Thread.sleep(300);
			return true;
		}
		else
		{
			return false;
		}
	}
	public boolean ChildCustomer() throws InterruptedException
	{
		WaitElement.WaitForElement(customer, 300, Driver);
		customer.click();
		WaitElement.WaitForElement(childcustomers, 300, Driver);
		
		if(childcustomers.isDisplayed())
		{
			childcustomers.click();
			return true;
		}
		else {
			return false;
		}
	}
	public void addnew() throws InterruptedException
	{

		WaitElement.WaitForElement(customer, 300, Driver);
		customer.click();
		Thread.sleep(3000);
		WaitElement.WaitForElement(childcustomers, 300, Driver);
		childcustomers.click();
		Thread.sleep(300);
		WaitElement.WaitForElement(addnew, 300, Driver);
		addnew.click();
	}
	public void email(String Email) throws InterruptedException
	{
		WaitElement.WaitForElement(email, 300, Driver);
		email.sendKeys(Email);	
		Thread.sleep(300);
	}
	public void password(String Password) throws InterruptedException
	{
		WaitElement.WaitForElement(password, 300, Driver);
		password.sendKeys(Password);
		Thread.sleep(300);
	}
	public void fname(String Fname) throws InterruptedException {
		WaitElement.WaitForElement(firstname, 300, Driver);
		firstname.sendKeys(Fname);
		Thread.sleep(300);
	}
	public void lname(String Lname) throws InterruptedException
	{
		WaitElement.WaitForElement(lastname, 300, Driver);
		lastname.sendKeys(Lname);
		Thread.sleep(300);
	}
	public void gender() throws InterruptedException
	{
		WaitElement.WaitForElement(male, 300, Driver);
		male.click();
		Thread.sleep(300);
	}
	public void dob() throws InterruptedException
	{
		WaitElement.WaitForElement(dob, 300, Driver);
		dob.sendKeys("5/24/1992");
		JavascriptExecutor js= ((JavascriptExecutor)Driver);
			js.executeScript("arguments[0].scrollIntoView(true);", vendor);
			Thread.sleep(300);
	}
	public void comapnyname(String CompanyName) throws InterruptedException
	{
		WaitElement.WaitForElement(company, 300, Driver);
		company.sendKeys(CompanyName);
		Thread.sleep(300);
	}
	public void crole() throws AWTException, InterruptedException
	{
		WaitElement.WaitForElement(select, 300, Driver);
		select.click();
		Robot r1 = new Robot();
		r1.keyPress(KeyEvent.VK_DOWN);	
		r1.keyPress(KeyEvent.VK_ENTER);
		r1.keyRelease(KeyEvent.VK_DOWN);
		r1.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
	}
	public void vendors() throws AWTException, InterruptedException
	{
		WaitElement.WaitForElement(vendor, 300, Driver);
		vendor.click();
		Robot r2=new Robot();
		r2.keyPress(KeyEvent.VK_DOWN);
		r2.keyPress(KeyEvent.VK_ENTER);
		r2.keyPress(KeyEvent.VK_DOWN);
		r2.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		JavascriptExecutor js2 =(JavascriptExecutor)Driver;
		js2.executeScript("scroll(0,-1000)");
		Thread.sleep(3000);
	}
	public void save() throws InterruptedException
	{
		WaitElement.WaitForElement(save, 300, Driver);
		save.click();
		Thread.sleep(5000);
	}
	public boolean testaddcustomer()
	{
		String expurl="https://admin-demo.nopcommerce.com/Admin/Customer/List";
		String cururl=Driver.getCurrentUrl();
		if(expurl.equals(cururl))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public void ChildCustomerClick() throws InterruptedException
	{
		WaitElement.WaitForElement(customer, 300, Driver);
		customer.click();
		WaitElement.WaitForElement(childcustomers, 300, Driver);
		childcustomers.click();
		Thread.sleep(3000);
	}
	public void SearchWithFirstname() throws InterruptedException
	{
		WaitElement.WaitForElement(seachfirstname, 3000, Driver);
		seachfirstname.sendKeys("John");
		WaitElement.WaitForElement(searchbtn, 3000, Driver);
		searchbtn.click();
		Thread.sleep(3000);
	}
	
	public void SearchWithEmail() throws InterruptedException
	{
		WaitElement.WaitForElement(searchemail, 300, Driver);;
		searchemail.sendKeys("abc@def.coms");
		WaitElement.WaitForElement(searchbtn, 3000, Driver);
		searchbtn.click();
		Thread.sleep(3000);
	}
	
	public void SearchWithLastname() throws InterruptedException
	{
		WaitElement.WaitForElement(seachlastname, 300, Driver);
		seachlastname.sendKeys("Smith");
		WaitElement.WaitForElement(searchbtn, 300, Driver);
		searchbtn.click();
		Thread.sleep(3000);
	}
	
	public void SearchWithCompany() throws InterruptedException
	{
		WaitElement.WaitForElement(searchcompany, 300, Driver);
		searchcompany.sendKeys("Ricoh");
		WaitElement.WaitForElement(searchbtn, 300, Driver);
		searchbtn.click();
		Thread.sleep(3000);
	}
	
	public boolean CustomerRolesOption() throws InterruptedException
	{
		WaitElement.WaitForElement(customer, 300, Driver);
		if(customerole.isDisplayed())
		{
			customerole.click();
			Thread.sleep(3000);
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void AddCutomerRole() throws InterruptedException
	{
		WaitElement.WaitForElement(customer, 300, Driver);
		customer.click();
		WaitElement.WaitForElement(customerole, 300, Driver);
		customerole.click();
		Thread.sleep(3000);
		WaitElement.WaitForElement(addrole, 300, Driver);
		addrole.click();
		Thread.sleep(3000);
		rolename.sendKeys("New Role");
		roleshipping.click();
		taxrole.click();
		savebtn.click();
		Thread.sleep(3000);
	}
}
