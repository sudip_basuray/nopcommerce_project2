package com.nopcommerce.utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitElement 
{
	public static void WaitForElement(WebElement element , int WaitTime, WebDriver driver)
	{
		WebDriverWait wait = new WebDriverWait(driver , WaitTime);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public static void waitTill(int WaitTime)
	{
		try {
			Thread.sleep(WaitTime);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
}
