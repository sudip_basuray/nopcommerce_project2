package a_project_nopcommerce;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.nopcommerce.utility.BrowserFactory;



public class CustomerSuite
{
	WebDriver driver;
	BrowserFactory browserfactory;
	LoginPage loginpage;
	CustomersPage custpage;
	
	@BeforeMethod
	public void Applaunch()
	{
		driver=BrowserFactory.StartWebBrowser("chrome", "https://admin-demo.nopcommerce.com");
	}
	
	@Test(priority=0,enabled=true,description="TC_003:To Check Wheather the Customers Tab is clickable or not")
	public void CustomerClick()
	{
		try 
		{
			loginpage=PageFactory.initElements(driver, LoginPage.class);
			loginpage.usernameEntry("admin@yourstore.com");
			loginpage.passwordentry("admin");
			loginpage.loginbuttonclick();
			custpage=PageFactory.initElements(driver, CustomersPage.class);
		
			Assert.assertEquals(true, custpage.CustomerOption());
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
	@Test(priority=1,enabled=true,description="TC_004:To Check Wheather the Child Customers Tab is clickable or not")
	public void ChildCustomer()
	{
		try 
		{
			loginpage=PageFactory.initElements(driver, LoginPage.class);
			loginpage.usernameEntry("admin@yourstore.com");
			loginpage.passwordentry("admin");
			loginpage.loginbuttonclick();
			custpage=PageFactory.initElements(driver, CustomersPage.class);
		
			Assert.assertEquals(true, custpage.ChildCustomer());
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
	@Test(priority=3,enabled=true,description="TC_005 : adding customer")
	public void Addcustomer()
	{
		try
		{
			loginpage=PageFactory.initElements(driver, LoginPage.class);
			loginpage.usernameEntry("admin@yourstore.com");
			loginpage.passwordentry("admin");
			loginpage.loginbuttonclick();
			custpage=PageFactory.initElements(driver, CustomersPage.class);
			custpage.addnew();
			custpage.email("sbr@gmail.com");
			custpage.password("password");
			custpage.fname("abc");
			custpage.lname("def");
			custpage.gender();
			custpage.dob();
			custpage.comapnyname("Ricoh");
			custpage.crole();	
			custpage.vendors();	
			custpage.save();	
			Thread.sleep(3000);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
	@Test(priority=4,enabled=true,description="TC_006 : Searching customer With First Name")
	public void CustomerSearchFname()
	{
		try
		{
			loginpage=PageFactory.initElements(driver, LoginPage.class);
			loginpage.usernameEntry("admin@yourstore.com");
			loginpage.passwordentry("admin");
			loginpage.loginbuttonclick();
			custpage=PageFactory.initElements(driver, CustomersPage.class);
			custpage.ChildCustomerClick();
			custpage.SearchWithFirstname();
			
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	@Test(priority=5,enabled=true,description="TC_007 : Searching customer With First Name")
	public void CustomerSearchLname()
	{
		try
		{
			loginpage=PageFactory.initElements(driver, LoginPage.class);
			loginpage.usernameEntry("admin@yourstore.com");
			loginpage.passwordentry("admin");
			loginpage.loginbuttonclick();
			custpage=PageFactory.initElements(driver, CustomersPage.class);
			custpage.ChildCustomerClick();
			custpage.SearchWithLastname();
			
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	@Test(priority=6,enabled=true,description="TC_007 : Searching customer With First Name")
	public void CustomerSearchEmail()
	{
		try
		{
			loginpage=PageFactory.initElements(driver, LoginPage.class);
			loginpage.usernameEntry("admin@yourstore.com");
			loginpage.passwordentry("admin");
			loginpage.loginbuttonclick();
			custpage=PageFactory.initElements(driver, CustomersPage.class);
			custpage.ChildCustomerClick();
			custpage.SearchWithEmail();;
			
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	@Test(priority=7,enabled=true,description="TC_008 : Searching customer With Company Name")
	public void CustomerSearchComapany()
	{
		try
		{
			loginpage=PageFactory.initElements(driver, LoginPage.class);
			loginpage.usernameEntry("admin@yourstore.com");
			loginpage.passwordentry("admin");
			loginpage.loginbuttonclick();
			custpage=PageFactory.initElements(driver, CustomersPage.class);
			custpage.ChildCustomerClick();
			custpage.SearchWithCompany();
			
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
	@Test(priority=8,enabled=true,description="TC_010:To Check Wheather the Customers roles Tab is clickable or not")
	public void CustomerRoles()
	{
		try 
		{
			loginpage=PageFactory.initElements(driver, LoginPage.class);
			loginpage.usernameEntry("admin@yourstore.com");
			loginpage.passwordentry("admin");
			loginpage.loginbuttonclick();
			custpage=PageFactory.initElements(driver, CustomersPage.class);
			custpage.ChildCustomerClick();
			Assert.assertEquals(true, custpage.CustomerRolesOption());
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
	@Test(priority=9,enabled=true,description="TC_010:Add Customers roles ")
	public void AddCustomerRole() throws InterruptedException
	{
		loginpage=PageFactory.initElements(driver, LoginPage.class);
		loginpage.usernameEntry("admin@yourstore.com");
		loginpage.passwordentry("admin");
		loginpage.loginbuttonclick();
		custpage=PageFactory.initElements(driver, CustomersPage.class);
		custpage.AddCutomerRole();
	}
	
	@AfterMethod
	public void appclose()
	{
		driver.close();
	}
	
	
}
