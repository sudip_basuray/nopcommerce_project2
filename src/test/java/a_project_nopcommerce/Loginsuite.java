package a_project_nopcommerce;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.nopcommerce.utility.BrowserFactory;


public class Loginsuite 
{
	BrowserFactory browserfactory ;
	LoginPage loginpage;
	WebDriver driver;
	
	
	@BeforeMethod
	
		public void applaunch()
		{
			driver=BrowserFactory.StartWebBrowser("chrome", "https://admin-demo.nopcommerce.com");
		}
	/*
	 * TC_001 successful login
	 */
	
	@Test(priority=0,enabled=true,description="TC_001 :Verify_Successful_Login_with_Valid_Credentials")
	
	public void AppLogin()
	{
		try {
			loginpage=PageFactory.initElements(driver, LoginPage.class);
			loginpage.usernameEntry("admin@yourstore.com");
			loginpage.passwordentry("admin");
			loginpage.loginbuttonclick();
			
			Assert.assertEquals(true, loginpage.verifyvalidlogin());		
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
@Test(priority=1,enabled=true,description="TC_002:Verify_Unsuccessful_Login_with_Invalid Credentials")
	
	public void invalidlogin()
	{
		try {
			loginpage=PageFactory.initElements(driver, LoginPage.class);
			loginpage.usernameEntry("admin@");
			loginpage.passwordentry("admin");
			loginpage.loginbuttonclick();
			
			Assert.assertEquals(true, loginpage.verifyinvalidlogin());		
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
	@AfterMethod
	public void appclose()
	{
		driver.close();
	}
	
}
